#
#  Andrew McNab, University of Manchester.
#  Copyright (c) 2016. All rights reserved.
#
#  Redistribution and use in source and binary forms, with or
#  without modification, are permitted provided that the following
#  conditions are met:
#
#    o Redistributions of source code must retain the above
#      copyright notice, this list of conditions and the following
#      disclaimer. 
#    o Redistributions in binary form must reproduce the above
#      copyright notice, this list of conditions and the following
#      disclaimer in the documentation and/or other materials
#      provided with the distribution. 
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
#  CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
#  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
#  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
#  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
#  TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
#  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
#  OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#  POSSIBILITY OF SUCH DAMAGE.
#

include VERSION

INSTALL_FILES=VERSION mjf.init \
              prologue.user epilogue.user \
              mjf.sh.torque mjf.csh.torque mjf-get-total-cpu.torque \
              mjf.sh.htcondor mjf.csh.htcondor mjf-get-total-cpu.htcondor \
              mjf-job-wrapper make-jobfeatures.htcondor \
              mjf.sh.gridengine mjf.csh.gridengine make-jobfeatures.gridengine \
              mjf.sh.onlymf mjf.csh.onlymf \
              DIRACbenchmark.py db12.init

TGZ_FILES=$(INSTALL_FILES) Makefile mjf-torque.spec mjf-htcondor.spec \
          mjf-gridengine.spec mjf-onlymf.spec mjf-db12.spec README

GNUTAR ?= tar
mjf-scripts.tgz: $(TGZ_FILES)
	mkdir -p TEMPDIR/mjf-scripts
	cp $(TGZ_FILES) TEMPDIR/mjf-scripts
	cd TEMPDIR ; $(GNUTAR) zcvf ../mjf-scripts.tgz --owner=root --group=root mjf-scripts
	rm -R TEMPDIR

install: $(INSTALL_FILES)
	mkdir -p $(RPM_BUILD_ROOT)/etc/rc.d/init.d \
                 $(RPM_BUILD_ROOT)/etc/profile.d 
	cp mjf.init \
           $(RPM_BUILD_ROOT)/etc/rc.d/init.d/mjf

onlymf-install: $(INSTALL_FILES) install
	cp mjf.sh.onlymf \
           $(RPM_BUILD_ROOT)/etc/profile.d/mjf.sh
	cp mjf.csh.onlymf \
           $(RPM_BUILD_ROOT)/etc/profile.d/mjf.csh
	
onlymf-rpm: mjf-scripts.tgz
	rm -Rf RPMTMP
	mkdir -p RPMTMP/SOURCES RPMTMP/SPECS RPMTMP/BUILD \
         RPMTMP/SRPMS RPMTMP/RPMS/noarch RPMTMP/BUILDROOT
	cp -f mjf-scripts.tgz RPMTMP/SOURCES        
	export MJF_VERSION=$(VERSION) ; rpmbuild -ba \
	  --define "_topdir $(shell pwd)/RPMTMP" \
	  --buildroot $(shell pwd)/RPMTMP/BUILDROOT mjf-onlymf.spec

torque-install: $(INSTALL_FILES) install
	mkdir -p $(RPM_BUILD_ROOT)/usr/sbin 
	cp mjf.sh.torque \
           $(RPM_BUILD_ROOT)/etc/profile.d/mjf.sh
	cp mjf.csh.torque \
           $(RPM_BUILD_ROOT)/etc/profile.d/mjf.csh
	mkdir -p $(RPM_BUILD_ROOT)/var/lib/torque/mom_priv
	cp prologue.user epilogue.user \
           $(RPM_BUILD_ROOT)/var/lib/torque/mom_priv
	cp mjf-get-total-cpu.torque \
           $(RPM_BUILD_ROOT)/usr/sbin/mjf-get-total-cpu
	
torque-rpm: mjf-scripts.tgz
	rm -Rf RPMTMP
	mkdir -p RPMTMP/SOURCES RPMTMP/SPECS RPMTMP/BUILD \
         RPMTMP/SRPMS RPMTMP/RPMS/noarch RPMTMP/BUILDROOT
	cp -f mjf-scripts.tgz RPMTMP/SOURCES        
	export MJF_VERSION=$(VERSION) ; rpmbuild -ba \
	  --define "_topdir $(shell pwd)/RPMTMP" \
	  --buildroot $(shell pwd)/RPMTMP/BUILDROOT mjf-torque.spec

htcondor-install: $(INSTALL_FILES) install
	mkdir -p $(RPM_BUILD_ROOT)/usr/sbin 
	cp mjf.sh.htcondor \
           $(RPM_BUILD_ROOT)/etc/profile.d/mjf.sh
	cp mjf.csh.htcondor \
           $(RPM_BUILD_ROOT)/etc/profile.d/mjf.csh
	cp mjf-job-wrapper \
           $(RPM_BUILD_ROOT)/usr/sbin
	cp make-jobfeatures.htcondor \
           $(RPM_BUILD_ROOT)/usr/sbin/make-jobfeatures
	cp mjf-get-total-cpu.htcondor \
           $(RPM_BUILD_ROOT)/usr/sbin/mjf-get-total-cpu

htcondor-rpm: mjf-scripts.tgz
	rm -Rf RPMTMP
	mkdir -p RPMTMP/SOURCES RPMTMP/SPECS RPMTMP/BUILD \
         RPMTMP/SRPMS RPMTMP/RPMS/noarch RPMTMP/BUILDROOT
	cp -f mjf-scripts.tgz RPMTMP/SOURCES        
	export MJF_VERSION=$(VERSION) ; rpmbuild -ba \
	  --define "_topdir $(shell pwd)/RPMTMP" \
	  --buildroot $(shell pwd)/RPMTMP/BUILDROOT mjf-htcondor.spec

gridengine-install: $(INSTALL_FILES) install
	mkdir -p $(RPM_BUILD_ROOT)/usr/sbin 
	cp mjf.sh.gridengine \
           $(RPM_BUILD_ROOT)/etc/profile.d/mjf.sh
	cp mjf.csh.gridengine \
           $(RPM_BUILD_ROOT)/etc/profile.d/mjf.csh
	cp make-jobfeatures.gridengine \
           $(RPM_BUILD_ROOT)/usr/sbin/make-jobfeatures

gridengine-rpm: mjf-scripts.tgz
	rm -Rf RPMTMP
	mkdir -p RPMTMP/SOURCES RPMTMP/SPECS RPMTMP/BUILD \
         RPMTMP/SRPMS RPMTMP/RPMS/noarch RPMTMP/BUILDROOT
	cp -f mjf-scripts.tgz RPMTMP/SOURCES        
	export MJF_VERSION=$(VERSION) ; rpmbuild -ba \
	  --define "_topdir $(shell pwd)/RPMTMP" \
	  --buildroot $(shell pwd)/RPMTMP/BUILDROOT mjf-gridengine.spec

db12-install: $(INSTALL_FILES)
	mkdir -p $(RPM_BUILD_ROOT)/etc/rc.d/init.d \
                 $(RPM_BUILD_ROOT)/etc/db12 \
                 $(RPM_BUILD_ROOT)/usr/sbin 
	cp db12.init \
           $(RPM_BUILD_ROOT)/etc/rc.d/init.d/db12
	cp DIRACbenchmark.py \
           $(RPM_BUILD_ROOT)/usr/sbin

db12-rpm: mjf-scripts.tgz
	rm -Rf RPMTMP
	mkdir -p RPMTMP/SOURCES RPMTMP/SPECS RPMTMP/BUILD \
         RPMTMP/SRPMS RPMTMP/RPMS/noarch RPMTMP/BUILDROOT
	cp -f mjf-scripts.tgz RPMTMP/SOURCES        
	export MJF_VERSION=$(VERSION) ; rpmbuild -ba \
	  --define "_topdir $(shell pwd)/RPMTMP" \
	  --buildroot $(shell pwd)/RPMTMP/BUILDROOT mjf-db12.spec
