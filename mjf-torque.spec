Name: mjf-torque
Version: %(echo ${MJF_VERSION:-0.0})
Release: 1
BuildArch: noarch
Summary: Machine/Job Features for Torque/PBS
License: BSD
Group: System Environment/Daemons
Source: mjf-scripts.tgz
Vendor: GridPP
Packager: Andrew McNab <Andrew.McNab@cern.ch>

%description
MJF implementations following HSF-TN-2016-02

%prep

%setup -n mjf-scripts

%build

%install
make torque-install

%post
chkconfig mjf on
service mjf start

%preun
if [ "$1" = "0" ] ; then
  # if uninstallation rather than upgrade then stop
  chkconfig mjf off
  service mjf stop
fi

%files
/var/lib/torque/mom_priv
/etc/rc.d/init.d/*
/etc/profile.d/*
/usr/sbin/*
