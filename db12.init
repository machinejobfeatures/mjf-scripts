#!/bin/bash
#
# db12		Run DB12 fast benchmark and create /etc/db12 files
#
# chkconfig: 345 01 99
# description: Run DB12 fast benchmark and create /etc/db12 files

# Source function library.
. /etc/init.d/functions

# If total_cpu is already created (in Kickstart?), we use that:
#
if [ -r /etc/db12/total_cpu ] ; then
  total_cpu=`cat /etc/db12/total_cpu`
fi

if [ "$total_cpu" == "" ] ; then
  # If total_cpu not given in /etc/db12/total_cpu then count OS processors
  total_cpu=`grep '^processor[[:space:]]*:' /proc/cpuinfo | wc --lines`
  mkdir -p /etc/db12
  echo "$total_cpu" > /etc/db12/total_cpu
fi

if [ -r /etc/db12/iterations ] ; then
  iterations=`cat /etc/db12/iterations`
fi

if [ "$iterations" == "" ] ; then
  iterations=1
  echo "$iterations" >/etc/db12/iterations
fi

start() {
	[ "$EUID" != "0" ] && exit 1

        echo -n $"Run DB12 fast benchmark: "
        
        if [ -x /bin/plymouth ] ; then
         # Plymouth boot up splash screen uses lots of CPU!
         plymouth hide-splash
        fi
        
        # Try to use full CPU speed for benchmarking
        modprobe --quiet acpi-cpufreq
        for i in /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor ; do echo performance > $i ; done

        # Get the total benchmark for this machine, assuming $total_cpu jobslots 
        db12_sum=`/usr/sbin/DIRACbenchmark.py --iterations=$iterations --extra-iteration $total_cpu | head -1 | cut -f2 -d' '`

        if [ "$db12_sum" != "" ] ; then
          echo "$db12_sum" > /etc/db12/db12
        fi

        # Set the governor to ondemand, in case run outside the SysV boot and not followed by cpuspeed
        for i in /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor ; do echo ondemand > $i ; done

        echo_success
        echo
	return 0
}

stop() {
	[ "$EUID" != "0" ] && exit 3

        echo -n $"Dummy DB12 stop action: "
        echo_success
        echo
	return 0
}

case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  restart|force-reload|reload)
        stop
	start
	;;
  *)
	echo $"Usage: $0 {start|stop|restart|force-reload|reload}"
	exit 4
esac
